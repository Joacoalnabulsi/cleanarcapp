package com.example.domain.model


data class NetworkResponse(
    val info: Info,
    val results: List<Character>
)