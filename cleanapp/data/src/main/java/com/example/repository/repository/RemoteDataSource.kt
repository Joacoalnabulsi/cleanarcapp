package com.example.repository.repository

import com.example.domain.model.NetworkResponse


interface RemoteDataSource {
    suspend fun getAllCharacters():NetworkResponse
}
