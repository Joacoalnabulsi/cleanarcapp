package com.example.repository.repository

import com.example.domain.model.NetworkResponse
import com.example.repository.repository.RemoteDataSource


interface Repository {
    suspend fun getAllCharacters():NetworkResponse
}