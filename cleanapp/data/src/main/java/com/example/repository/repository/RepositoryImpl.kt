package com.example.repository.repository


class RepositoryImpl( val remoteDataSource: RemoteDataSource):Repository {
    override suspend fun getAllCharacters() = remoteDataSource.getAllCharacters()
}