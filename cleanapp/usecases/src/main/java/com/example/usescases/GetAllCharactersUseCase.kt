package com.example.usescases

import com.example.repository.repository.Repository

class GetAllCharactersUseCase( val repository: Repository) {
    suspend operator fun invoke() = repository.getAllCharacters()
}
