package com.example.cleanarcapp.di

import com.example.cleanarcapp.data.network.RetrofitService
import com.example.cleanarcapp.data.network.RickAndMortyApiService
import com.example.cleanarcapp.utils.BASE_URL
import com.example.repository.repository.RemoteDataSource
import com.example.repository.repository.Repository
import com.example.repository.repository.RepositoryImpl
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val dataModule = module {
    single {
        Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    single { get<Retrofit>().create<RickAndMortyApiService>() }
    single<RemoteDataSource> { RetrofitService(get<RickAndMortyApiService>()) }
    factory<Repository> { RepositoryImpl(get<RemoteDataSource>()) }

}