package com.example.cleanarcapp.data.network

import com.example.cleanarcapp.utils.BASE_URL
import com.example.domain.model.NetworkResponse
import com.example.repository.repository.RemoteDataSource
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



class RetrofitService(val apiService: RickAndMortyApiService):RemoteDataSource {
    //fun provideRetrofitInstance(): Retrofit =
      //  Retrofit.Builder().baseUrl(BASE_URL)
      //      .addConverterFactory(GsonConverterFactory.create())
      //      .build()


    //fun provideRickAndMortyApiService(retrofit: Retrofit): RickAndMortyApiService =
       // retrofit.create(RickAndMortyApiService::class.java)

    override suspend fun getAllCharacters(): NetworkResponse {
       return apiService.getCharacters().body()!!
    }
}