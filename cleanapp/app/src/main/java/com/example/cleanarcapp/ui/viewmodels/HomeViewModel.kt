package com.example.cleanarcapp.ui.viewmodels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.usescases.GetAllCharactersUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import com.example.domain.model.Character

class HomeViewModel( val getAllCharactersUseCase: GetAllCharactersUseCase) : ViewModel() {

    private var _charactersStateFlow: MutableStateFlow<List<Character>> = MutableStateFlow(emptyList())
    val charactersStateFlow: StateFlow<List<Character>> get() = _charactersStateFlow

    fun getAllCharacters() {
        viewModelScope.launch {
            try {
                val characters = getAllCharactersUseCase()
                _charactersStateFlow.value = characters.results
            } catch (e: Exception) {
                // Handle the exception
            }
        }
    }
}