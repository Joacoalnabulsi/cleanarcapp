package com.example.cleanarcapp

import android.app.Application
import com.example.cleanarcapp.di.dataModule
import com.example.cleanarcapp.di.useCaseModule
import com.example.cleanarcapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin


class CleanArcApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            // Reference Android context
            androidContext(this@CleanArcApp)

            // Load modules
            modules(dataModule, useCaseModule, viewModelModule)
        }
    }
}
