package com.example.cleanarcapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cleanarcapp.databinding.ItemHomeAdapterBinding
import com.example.domain.model.Character

class HomeAdapter(private var characters: List<Character>) : RecyclerView.Adapter<HomeAdapter.CharacterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val itemBinding =
            ItemHomeAdapterBinding.inflate(
                LayoutInflater.from(parent.context), parent,
                false
            )
        return CharacterViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val currentItem = characters[position]
        holder.bind(currentItem)
    }

    fun updateData(newList: List<Character>) {
        characters = newList
        notifyDataSetChanged()
    }

    inner class CharacterViewHolder(private val binding: ItemHomeAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(character: Character) {
            binding.characterName.text = character.name
        }
    }
}
