package com.example.cleanarcapp.data.network

import com.example.domain.model.NetworkResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RickAndMortyApiService {
    @GET("character")
    suspend fun getCharacters(): Response<NetworkResponse>
}