package com.example.cleanarcapp.di

import com.example.cleanarcapp.ui.viewmodels.HomeViewModel
import com.example.usescases.GetAllCharactersUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        HomeViewModel(getAllCharactersUseCase = get<GetAllCharactersUseCase>())
    }
}