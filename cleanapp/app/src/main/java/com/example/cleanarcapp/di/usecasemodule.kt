package com.example.cleanarcapp.di

import com.example.repository.repository.Repository
import com.example.usescases.GetAllCharactersUseCase
import org.koin.dsl.module

val useCaseModule = module {
    single { GetAllCharactersUseCase(repository = get<Repository>()) }
}